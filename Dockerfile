FROM registry.fedoraproject.org/fedora:37

COPY nginx/ /etc/nginx/
COPY --chmod=0755 entrypoints.d/* /entrypoints.d/
COPY --chmod=0755 entrypoint.sh /

ARG LOCAL_CACHE

RUN dnf install --setopt install_weak_deps=false --setopt keepcache=true -y nginx-all-modules openssl python3-jinja2 \
    && if [[ -n "$LOCAL_CACHE" ]] ; then true; else dnf clean all; fi \
    && if [[ -n "$LOCAL_CACHE" ]] ; then true; else rm -rf /var/cache/dnf; fi \
    && curl -sSL https://ssl-config.mozilla.org/ffdhe2048.txt > /etc/ssl/dhparam \
    && mkdir -p /etc/nginx/http.d \
    && mkdir -p /etc/nginx/stream.d \
    && mkdir -p /entrypoints.d \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && chmod +x /entrypoint.sh \
    && chmod +x /entrypoints.d/*

EXPOSE 80/tcp
EXPOSE 443/tcp

ENV SSL_CONFIG="MOZILLA_HIGH"

ENTRYPOINT ["/bin/bash"]
CMD ["/entrypoint.sh"]