#!/bin/bash

for f in /entrypoints.d/*; do
  $f
done

nginx -g 'daemon off;'