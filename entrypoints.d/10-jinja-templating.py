#!/usr/bin/env python3

import jinja2
import os
from glob import glob

environment = jinja2.Environment(lstrip_blocks=True)

for path in glob("/etc/nginx/**.j2"):
    with open(path) as f:
        template = jinja2.Template(f.read())

    with open(path.strip(".j2"), "w") as f:
        f.write(template.render(env=os.environ))
