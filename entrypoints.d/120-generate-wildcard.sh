if [ ! -f /etc/pki/tls/private/wildcard.key ]
then
    openssl req -x509 -newkey ec:<(openssl ecparam -name secp384r1) -keyout /etc/pki/tls/private/wildcard.key -out /etc/pki/tls/certs/wildcard.crt -sha256 -days 3650 -nodes -subj '/CN=*'
fi
