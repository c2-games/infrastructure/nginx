#!/bin/bash

mkdir -p .dnfcache

podman run \
    -v $(pwd):/app:Z \
    -v $(pwd)/.dnfcache:/var/cache/dnf:Z \
    gcr.io/kaniko-project/executor:debug --no-push --context /app --dockerfile /app/Dockerfile --build-arg LOCAL_CACHE=true 
